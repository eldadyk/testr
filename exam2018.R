install.packages("ggplot2")
library(ggplot2)

#------Linear Model------
install.packages('caTools')
library(caTools)

install.packages('dplyr')
library(dplyr)

#------Missing values------
install.packages('Amelia')
library(Amelia)

#------Correlations------ 
install.packages("corrplot")
library(corrplot)

#------Kmeans algorithm------
install.packages('cluster')
library(cluster)

#------SQL------
install.packages('RSQLite')
library(RSQLite)

install.packages('sqldf')
library(sqldf)

#------Text mining / Naive base------

#tm is a text mining package with a bunch of text mining functions
install.packages('tm')
library(tm)

install.packages('e1071')
library(e1071)

#genrating a word cloud of spam and not spam 
install.packages('wordcloud')
library(wordcloud)

#creating confision matrix
install.packages('SDMTools')
library(SDMTools)

install.packages('RColorBrewer')
library(RColorBrewer)

install.packages('NLP')
library('NLP')

#------Decision trees------
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('randomForest')
library(randomForest)

#------confusion matrix------
install.packages('ISLR')
library('ISLR')

#------ROC curve------
install.packages('pROC')
library(pROC)

#------Other Useful------
install.packages(c("caret","quanteda","irlba"))


#------q1------------------------------------------------------------------------------------------------


loan  <- read.csv('test.csv', stringsAsFactors = FALSE)
str(loan)
View(loan)

#var target: not.fully.paid

# Making purpose a factor 
loan$purpose <- as.factor(loan$purpose)
loan$purpose <- factor(loan$purpose, levels = c("all_other", "credit_card", "debt_consolidation", "educational","home_improvement","major_purchase","small_business" ), labels = c(1,2,3,4,5,6,7) )
View(loan)
str(loan)

#1 calculate number on rows with NA's 
sum(is.na(loan))

data[data=='?']<- NA#����� �� ���� ��� ����� ���� ���� �� �� ������ �� ���� ����
any(is.na(loan)) #true
library(Amelia)
missmap(loan)#����� �������� ����� �� �� �����
data <- na.omit(loan)
missmap(loan)#���� ����� ��� ����� ������� ���� ��� ��� ����� ��� ������


#2.Remove all the rows with NA's
#airquality.clean <- na.omit(airquality)
#nrow(airquality.clean)
#data("airquality")

# Check missing values 
any(is.na(loan))

#there is no missing values

#-----------------q2-------------------------------------------------------------------------------------------------------


#ggplot2
install.packages('ggplot2')
library(ggplot2)

#Q1 - A chart showing how intrest affects  Loan repayment
q1 <- ggplot(loan, aes(x = loan$int.rate, y = loan$not.fully.paid)) + geom_bin2d() + scale_fill_gradient(low = 'green', high = 'red') + ggtitle("how intrest affects the Loan repayment")
#How temterature affects the count? 
q2 <- ggplot(loan, aes(loan$int.rate,loan$not.fully.paid)) + geom_point(alpha = 0.8, aes(color = loan$int.rate))#��� ���������� ���� ���� ������ ����  ����
#check the correlation between count and temp
cor(loan[,c('int.rate', 'not.fully.paid')])#the correlation is 0.15  which means no so high

#Q2 - A chart showing how purpose affects Loan repayment (use here a boxplot) learn here about what a boxplot is and how to interpret it (turn season into factor first, add labels to season (hint - the factor function has a label parameter)
levels(loan$purpose)
str(loan$purpose)
q3 <- ggplot(loan, aes(x = loan$purpose, y = loan$not.fully.paid)) + geom_boxplot() + ggtitle("how purpose affects loan repayment")


#Q3 - A chart showing how fico affectsLoan intrest
q1 <- ggplot(loan, aes(x = loan$int.rate, y = loan$fico)) + geom_bin2d() + scale_fill_gradient(low = 'green', high = 'red') + ggtitle("how intrest affects the fico")
q2 <- ggplot(loan, aes(x = loan$int.rate, y = loan$fico)) + geom_point(color = 'red') + ggtitle("how intrest affects the Loan repayment")


#----q4
confusion<- confusion.matrix(loan$not.fully.paid,loan$credit.policy ) #?????? ?????????????????? ????????????

TP <- confusion[2,2] #1,1 
FP <- confusion[2,1] #1,0 
TN <- confusion[1,1] #0,0 
FN <- confusion[1,2] #0,1 

recall <- TP/(TP+FN) #???
precision <- TP/(TP+FP) #??????


#------------------q3-----------------------------------------------------------------------------------------------


#q1
#spliting into training&test set
# the train set need to be 70% from the data and the test set need to be 30% from the data (randomize)

str(loan)
dim(loan)

split<-runif(9578)
#70% will be true
#train set
split<-split >0.3

#dividing raw data
train<- loan[split,]
test<- loan[!split,]

#coverting to Data Frame
df_train <- as.data.frame(train)
df_test<-as.data.frame(test)


#q2+q3
tree <- rpart(df_train$not.fully.paid ~ .,method = 'class' ,data = df_train)
prp(tree)

tree.preds <- predict(tree,df_test[,-14])





#-------------------------q4------------------------------------------------------------------------------------------


#Q1+q2 - Calculation of logistic regression model

#in logistic regression we need numeric values! 


log.model <- glm(not.fully.paid ~ . , family = binomial(link = 'logit'), df_train)
summary(log.model)

View(df_train)



#Q3+4- Logistic regression - confusion matrix
#-------------------------------------------------------------------------------------------------------------
  #get probabilities of prediction
predict.probabilities <- predict(log.model, df_test, type = 'response')
#get 0/1 of prediction
predicted.values<- ifelse(predict.probabilities > 0.5 ,1,0)
#prestict vector
pred.lg <- predicted.values

#creating confusion matrix
confusion.lg <- confusion.matrix(df_test$not.fully.paid,pred.lg)


TP <- confusion[2,2] #1,1 
FP <- confusion[2,1] #1,0 
TN <- confusion[1,1] #0,0 
FN <- confusion[1,2] #0,1 

recall <- TP/(TP+FN) 
precision <- TP/(TP+FP) 


#roccurve
#see only 0 records
a <- predict(log.model,df_test, type ='response')
b <- (1)
predictedprob.lg <-cbind(a,b)

predictedprob.lg[,'0']

install.packages('pROC')
library(pROC)

#building roc chart
rocCurve.cat <- roc(adtm.df$category, predictedprob.lg[,'0'], levels = c("economy","Sport"))
plot(rocCurve.cat, col="yellow", main='ROC chart')



#q5-----------------------------------------------------------------








